function saveVffForMathematica(varargin)

%   Author: Matt Latourette
%   Created: 12/29/2014
%   Department of Radiology
%   Michigan State University

    error(nargchk(0, 2, nargin))

    switch nargin
        case 0
            [filename pathname] = uigetfile({ ...
                '*.vff', 'All VFF Files (*.vff)'; ...
                '*.*', 'All Files (*.*)' ...
                }, 'Please select a vff file');
        case 1
            [filename pathname] = uigetfile({ ...
                '*.vff', 'All VFF Files (*.vff)'; ...
                '*.*', 'All Files (*.*)' ...
                }, 'Please select a vff file', varargin{1});
        case 2
            pathname = varargin{1};
            filename = varargin{2};
        otherwise
            error('VFF3DFileReader:ArgumentError', ...
                'ArgumentError:  Too many arguments specified');
    end

    vol = vff3D(pathname, filename);
    [~, fname, ~] = fileparts(filename);
    save(fullfile(pathname, [fname '.mat']), '-v6', 'vol');
end