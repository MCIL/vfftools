function [image] = vff3DGetFrame(volumeObject, frameNumber, showFrame)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    if(frameNumber < 1 || frameNumber > volumeObject.Frames)
        error('VFF3DGetFrame:ArgumentError', ...
            'Frame number out of range');
    end
    
    image = volumeObject.ImageStack(:,:,frameNumber);
    if (showFrame)
        h = figure;
        imshow(image, []);
        colormap(h, gray);
        set(h, 'Name', volumeObject.Filename);
    end
end

