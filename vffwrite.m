function [ output_args ] = vffwrite(image, header, varargin)
%   This program can be used to write a 2D VFF file suitable for
%   use with a GE Locuse RS micro CT system.
%
%   Matt Latourette, 6/9/2015
%   Department of Radiology
%   Michigan State University
%
%   [ output_args ] = vffwrite(image, header)
%
%   [ output_args ] = vffwrite(image, header, pathname)
%
%   [ output_args ] = vffwrite(image, header, pathname, filename)

error(nargchk(2, 4, nargin))

switch nargin
    case 2
        [filename pathname] = uiputfile({'*.vff', 'VFF file format'}, ...
            'Choose a path and file name');
    case 3
        [filename pathname] = uiputfile({'*.vff', 'VFF file format'}, ...
            'Choose a path and file name', ...
            fullfile(varargin{1}, 'output.vff'));
    case 4
        pathname = varargin{1};
        filename = varargin{2};
    otherwise
        error('VFFFileWriter:ArgumentError', ...
            'ArgumentError:  Too many arguments specified');
end

fid = fopen(fullfile(pathname, filename), 'w', 'b');

fprintf(fid, 'ncaa\n');

if (isfield(header, 'Rank') && isnumeric(header.Rank))
    fprintf(fid, 'rank=%u;\n', header.Rank);
end

if (isfield(header, 'Type'))
    fprintf(fid, 'type=%s;\n', header.Type);
end

if (isfield(header, 'Rows') && isfield(header, 'Columns') && ...
        isnumeric(header.Rows) && isnumeric(header.Columns))
    fprintf(fid, 'size=%u %u;\n', header.Rows, header.Columns);
end

if (isfield(header, 'RawSize') && isnumeric(header.RawSize))
    fprintf(fid, 'rawsize=%u;\n', header.RawSize);
end

if (isfield(header, 'Spacing') && ismatrix(header.Spacing) && ...
        isnumeric(header.Spacing))
    fprintf(fid, 'spacing=%#5.6f %#5.6f;\n', header.Spacing(1), ...
        header.Spacing(2));
end

if (isfield(header, 'Origin') && ismatrix(header.Origin) && ...
        isnumeric(header.Origin))
    fprintf(fid, 'origin=%#5.6f %#5.6f;\n', header.Origin(1), ...
        header.Origin(2));
end

if (isfield(header, 'Bands') && isnumeric(header.Bands))
    fprintf(fid, 'bands=%u;\n', header.Bands);
end

if (isfield(header, 'Bits') && isnumeric(header.Bits))
    fprintf(fid, 'bits=%u;\n', header.Bits);
end

if (isfield(header, 'Binning'))
    fprintf(fid, 'binning=%s;\n', header.Binning);
end

if (isfield(header, 'Format'))
    fprintf(fid, 'format=%s;\n', header.Format);
end

if (isfield(header, 'ElementSize') && isnumeric(header.ElementSize))
    fprintf(fid, 'elementsize=%#5.6f;\n', header.ElementSize);
end

if (isfield(header, 'Water') && isnumeric(header.Water))
    fprintf(fid, 'water=%#5.6f;\n', header.Water);
end

if (isfield(header, 'Air') && isnumeric(header.Air))
    fprintf(fid, 'air=%#5.6f;\n', header.Air);
end

if (isfield(header, 'BoneHU') && isnumeric(header.BoneHU))
    fprintf(fid, 'boneHU=%d;\n', header.BoneHU);
end

if (isfield(header, 'Subject'))
    fprintf(fid, 'subject=%s;\n', header.Subject);
end

if (isfield(header, 'Title'))
    fprintf(fid, 'title=%s;\n', header.Title);
end

if (isfield(header, 'Voltage') && isnumeric(header.Voltage))
    fprintf(fid, 'voltage=%u;\n', header.Voltage);
end

if (isfield(header, 'Current') && isnumeric(header.Current))
    fprintf(fid, 'current=%#5.1f;\n', header.Current);
end

if (isfield(header, 'Date'))
    fprintf(fid, 'date=%s;\n', header.Date);
end

% Form Feed terminates the file header
fprintf(fid, '\f\n');

fwrite(fid, image, 'int16', 0, 'b');

fclose(fid);