function [volumeObject] = vff3D(varargin)

%   This is a modification of the program described below
%
% %   This program can be used to open micro-CT VFF file in Matlab.
% %   
% %   Shanrong Zhang, 12/17/2004
% %   Department of Radiology, 
% %   University of Washington
% %   zhangs@u.washington.edu
% %
%
%   This alows 2D cross sections to be obtained from a 3D cube of data.
%   Easy modification will alow you to input 3D data into MatLAB from vff
%   files. Enjoy.
%
%   Samuel Lawman, 25/07/2008
%   School of Science and Technology
%   Nottingham Trent University
%
%   Additional modifications by:  Matt Latourette, 8/1/2013
%   Department of Radiology
%   Michigan State University

error(nargchk(0, 2, nargin))

switch nargin
    case 0
        [filename pathname] = uigetfile({ ...
            '*.vff', 'All VFF Files (*.vff)'; ...
            '*.*', 'All Files (*.*)' ...
            }, 'Please select a vff file');
    case 1
        [filename pathname] = uigetfile({ ...
            '*.vff', 'All VFF Files (*.vff)'; ...
            '*.*', 'All Files (*.*)' ...
            }, 'Please select a vff file', varargin{1});
    case 2
        pathname = varargin{1};
        filename = varargin{2};
    otherwise
        error('VFF3DFileReader:ArgumentError', ...
            'ArgumentError:  Too many arguments specified');
end

fid = fopen(fullfile(pathname, filename), 'r', 'b');

disp(filename)

done = false;
decimalRegex = '\d+(\.|(\.\d+))?';

volumeObject = struct('Path', {}, 'Filename', {}, 'Rows', {}, ...
    'Columns', {}, 'Frames', {}, 'Bits', {});
volumeObject(1).Path = pathname;
volumeObject(1).Filename = filename;

frame = 0;

while (~done)
    line = fgetl(fid);
    disp(line);
    
    if isempty(line) || ~isempty(find(uint8(line) == 12, 1))
        done = true;
        continue
    end
    
    temp = regexp(line, horzcat('^size=\s*(?<rows>\d+)\s+', ...
        '(?<columns>\d+)\s+(?<frames>\d+)\s*;\s*$'), 'names');
    if ~isempty(temp)
        volumeObject(1).Rows = str2double(temp.rows);
        volumeObject(1).Columns = str2double(temp.columns);
        volumeObject(1).Frames = str2double(temp.frames);
        continue
    end
    
    temp = regexp(line, '^bits=\s*(?<bits>\d+)\s*;\s*$', 'names');
    if ~isempty(temp)
        volumeObject(1).Bits = str2double(temp.bits);
        continue
    end

    temp = regexp(line, '^rank=\s*(?<rank>\d+)\s*;\s*$', 'names');
    if ~isempty(temp)
        volumeObject(1).Rank = str2double(temp.rank);
        continue
    end
    
    temp = regexp(line, '^type=\s*(?<type>.+)\s*;\s*$', 'names');
    if ~isempty(temp)
        volumeObject(1).Type = temp.type;
        continue
    end
    
    temp = regexp(line, horzcat('^origin=\s*', ...
        '(?<origin1>', decimalRegex, ')\s+', ...
        '(?<origin2>', decimalRegex, ')\s+', ...
        '(?<origin3>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        volumeObject(1).Origin = [str2double(temp.origin1) ...
            str2double(temp.origin2) str2double(temp.origin3)];
        continue
    end
    
    temp = regexp(line, '^y_bin=\s*(?<y_bin>\d+)\s*;\s*$', 'names');
    if ~isempty(temp)
        volumeObject(1).Y_Bin = str2double(temp.y_bin);
        continue
    end
    
    temp = regexp(line, '^z_bin=\s*(?<z_bin>\d+)\s*;\s*$', 'names');
    if ~isempty(temp)
        volumeObject(1).Z_Bin = str2double(temp.z_bin);
        continue
    end
    
    temp = regexp(line, '^bands=\s*(?<bands>\d+)\s*;\s*$', 'names');
    if ~isempty(temp)
        volumeObject(1).Bands = str2double(temp.bands);
        continue
    end
    
    temp = regexp(line, '^format=\s*(?<format>.+)\s*;\s*$', 'names');
    if ~isempty(temp)
        volumeObject(1).Format = temp.format;
        continue
    end
    
    temp = regexp(line, '^title=\s*(?<title>\w+)\s*;\s*$', 'names');
    if ~isempty(temp)
        volumeObject(1).Title = temp.title;
        continue
    end
    
    temp = regexp(line, '^subject=\s*(?<subject>\w+)\s*;\s*$', 'names');
    if ~isempty(temp)
        volumeObject(1).Subject = temp.subject;
        continue
    end
    
    temp = regexp(line, '^date=\s*(?<date>.+);\s*$', 'names');
    if ~isempty(temp)
        volumeObject(1).Date = temp.date;
        continue
    end
    
    temp = regexp(line, horzcat('^center_of_rotation=\s*', ...
        '(?<center_of_rotation>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        volumeObject(1).CenterOfRotation = str2double( ...
            temp.center_of_rotation);
        continue
    end
    
    temp = regexp(line, horzcat('^central_slice=\s*', ...
        '(?<central_slice>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        volumeObject(1).CentralSlice = str2double(temp.central_slice);
        continue
    end
    
    temp = regexp(line, horzcat('^rfan_y=\s*', ...
        '(?<rfan_y>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        volumeObject(1).RFan_Y = str2double(temp.rfan_y);
        continue
    end
    
    temp = regexp(line, horzcat('^rfan_z=\s*', ...
        '(?<rfan_z>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        volumeObject(1).RFan_Z = str2double(temp.rfan_z);
        continue
    end
    
    temp = regexp(line, horzcat('^angle_increment=\s*', ...
        '(?<angle_increment>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        volumeObject(1).AngleIncrement = str2double(temp.angle_increment);
        continue
    end
    
    temp = regexp(line, horzcat('^reverse_order=\s*', ... 
        '(?<reverse_order>\w+)\s*;\s*$'), 'names');
    if ~isempty(temp)
        volumeObject(1).ReverseOrder = temp.reverse_order;
        continue
    end
    
    temp = regexp(line, horzcat('^min=\s*', '(?<min>', decimalRegex, ...
        ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        volumeObject(1).Min = str2double(temp.min);
        continue
    end
    
    temp = regexp(line, horzcat('^max=\s*', '(?<max>', decimalRegex, ...
        ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        volumeObject(1).Max = str2double(temp.max);
        continue
    end
    
    temp = regexp(line, horzcat('^spacing=\s*', ...
        '(?<spacing1>', decimalRegex, ')\s+', ...
        '(?<spacing2>', decimalRegex, ')\s+', ...
        '(?<spacing3>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        volumeObject(1).Spacing = [str2double(temp.spacing1) ...
            str2double(temp.spacing2) str2double(temp.spacing3)];
        continue
    end
    
    temp = regexp(line, horzcat('^elementsize=\s*', ...
        '(?<elementsize>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        volumeObject(1).ElementSize = str2double(temp.elementsize);
        continue
    end
    
    temp = regexp(line, horzcat('^water=\s*', ...
        '(?<water>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        volumeObject(1).Water = str2double(temp.water);
        continue
    end
    
    temp = regexp(line, horzcat('^air=\s*', ...
        '(?<air>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        volumeObject(1).Air = str2double(temp.air);
        continue
    end
    
    temp = regexp(line, horzcat('^boneHU=\s*', ...
        '(?<boneHU>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        volumeObject(1).BoneHU = str2double(temp.boneHU);
        continue
    end
    
    temp = regexp(line, '^cmdLine=\s*"(?<cmdLine>.*)"\s*;\s*$', 'names');
    if ~isempty(temp)
        volumeObject(1).CmdLine = temp.cmdLine;
        continue
    end
end


% Assume data is in an integer format
enctyp = 'int';

imgdata = fread(fid, inf, [enctyp num2str(volumeObject.Bits)]);
fclose(fid);

% % As it turns out, the frame count given in the volumeObject for images from
% % the Locus eXplore micro-CT is incorrect (off by one).  So, just read 
% % to the eof and then figure out afterward how many frames we actually 
% % read.
unflipped = reshape(imgdata, volumeObject.Rows, volumeObject.Columns, []);
volumeObject(1).ImageStack = flipdim(permute(unflipped, [2 1 3]), 1);
volumeObject(1).Frames = size(volumeObject.ImageStack, 3);