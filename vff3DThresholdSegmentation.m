function [ output_args ] = vff3DThresholdSegmentation(threshold, varargin)
%vff3DThresholdSegmentation Segments a 3D VFF image volume with a threshold
%   Detailed explanation goes here
    argp = inputParser;
    argp.addRequired('Threshold', @isnumeric);
    argp.addOptional('Path', pwd, @ischar);
    argp.addOptional('Filename', blanks(0), @ischar);
    argp.addParamValue('Visualize', false, @islogical);
    argp.addParamValue('FaceColor', 'black', @(x) ischar(x) || ...
        (isvector(x) && ndims(x) == 3));
    argp.addParamValue('EdgeColor', 'none', @(x) ischar(x) || ...
        (isvector(x) && ndims(x) == 3));
    argp.addParamValue('XLim', [], @(x) ndims(x) == 2);
    argp.addParamValue('YLim', [], @(x) ndims(x) == 2);
    argp.addParamValue('ZLim', [], @(x) ndims(x) == 2);
    argp.addParamValue('Lighting', 'phong', @(x) ischar(x));
    
    % Parse and validate all input arguments
    argp.parse(threshold, varargin{:});
    
    if(isempty(p.Results.Filename))
        [filename pathname] = uigetfile({ ...
            '*.vff', 'All VFF Files (*.vff)'; ...
            '*.*', 'All Files (*.*)' ...
            }, 'Please select a vff file', p.Results.Path);
        p.Results.Path = pathname;
        p.Results.Filename = filename;
    end
    
    obj = vff3D(p.Results.Path, p.Results.Filename);
    vol = obj.ImageStack;
    if(isfield(obj, 'ElementSize'))
        voxelvolume = obj.ElementSize^3;
    else
        voxelvolume = obj.Spacing(1)*obj.Spacing(2)*obj.Spacing(3);
    end
end

