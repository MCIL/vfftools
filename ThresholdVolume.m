% function [ measuredvolume, segmentedvol, thresholdedvol, origvol ] = ...
%     ThresholdVolume(varargin)
function [ measuredvolume, segmentedvol, thresholdedvol, origvol] = ...
    ThresholdVolume(threshold, facecolor)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    
%     error(nargchk(2, 4, nargin))
%     
%     switch nargin
%         case 2
%             obj = vff3D();
%             threshold = varargin(1);
%             facecolor = varargin(2);
%         case 3
%             obj = vff3D(varargin(1));
%             threshold = varargin(2);
%             facecolor = varargin(3);
%         case 4
%             obj = vff3D(varargin(1), varargin(2));
%             threshold = varargin(3);
%             facecolor = varargin(4);
%         otherwise
%             error('ThresholdVolume:ArgumentError', ...
%                 'ArgumentError:  Wrong number of arguments specified');
%     end

    obj = vff3D();
    vol = obj.ImageStack;
    fv = isosurface(vol, threshold);
    p = patch(fv);
    isonormals(vol, p);
    set(p, 'FaceColor', facecolor, 'EdgeColor', 'none');
    daspect([1,1,1]);
    view(3);
    axis([0 100 0 100 0 100]);
    axis vis3d;
    grid on;
    set(gca, 'XTick', 0:20:100, 'YTick', 0:20:100, 'ZTick', 0:20:100);
    camlight;
    lighting phong;
    thresholdedvol = vol(vol>=threshold);   % Gives only the voxels above threshold
    segmentedvol = vol; % Initialize with original volume
    segmentedvol(vol<threshold) = -1000;    % Blank pixels below the threshold
    countvoxels = sum(reshape(vol, 1, [])>=threshold);
    if(isfield(obj, 'ElementSize'))
        voxelvolume = obj.ElementSize^3;
    else
        voxelvolume = obj.Spacing(1)*obj.Spacing(2)*obj.Spacing(3);
    end
    measuredvolume = countvoxels*voxelvolume;
    origvol = vol;
    % result is in mm^3 for eXplore Locus microCT data
end

