function [image, header] = vff(varargin)
%   This program can be used to open micro-CT VFF file in Matlab.
%   
%   Shanrong Zhang, 12/17/2004
%   Department of Radiology, 
%   University of Washington
%   zhangs@u.washington.edu
%
%   Modifications by:  Matt Latourette, 8/1/2013
%   Department of Radiology
%   Michigan State University
%
%   [IMAGE, HEADER] = vff() presents a file selection user interface so 
%   that the user may interactively select a VFF file to read.  The 
%   current filesystem path is used as the starting point.  The image is
%   then displayed.  The image data is returned in the IMAGE matrix.
%   Ancillary information from the VFF file header is returned in HEADER.
%
%   [IMAGE, HEADER] = vff(SHOWIMAGE) presents a file selection user 
%   interface so that the user may interactively select a VFF file to read.
%   The current filesystem path is used as the starting point.  The image
%   is then displayed if the Boolean value SHOWIMAGE is true.  Otherwise,
%   the image is not displayed.  The image data is returned in the IMAGE
%   matrix and the file header is returned in HEADER.
%
%   [IMAGE, HEADER] = vff(SHOWIMAGE, PATHNAME) presents a file selection
%   user interface so that the user may interactively select a VFF file to
%   read.  The PATHNAME specifies the starting point.  The image is then
%   displayed if the Boolean value SHOWIMAGE is true.  Otherwise, the
%   image is not displayed.  The image data is returned in the IMAGE matrix
%   and the file header is returned in HEADER.
%
%   [IMAGE, HEADER] = vff(SHOWIMAGE, PATHNAME, FILENAME) noninteractively
%   reads the VFF file specified by FILENAME from the filesystem path 
%   specified by PATHNAME.  The image is then displayed if the Boolean
%   value SHOWIMAGE is true.  Otherwise, the image is not displayed.  The
%   image data is returned in the IMAGE matrix and the file header is
%   returned in HEADER.

error(nargchk(0, 3, nargin))

switch nargin
    case 0
        showimage = 1;
        [filename pathname] = uigetfile({ ...
            '*.vff', 'All VFF Files (*.vff)'; ...
            '*.*', 'All Files (*.*)' ...
            }, 'Please select a vff file');
    case 1
        showimage = varargin{1};
        [filename pathname] = uigetfile({ ...
            '*.vff', 'All VFF Files (*.vff)'; ...
            '*.*', 'All Files (*.*)' ...
            }, 'Please select a vff file');
    case 2
        showimage = varargin{1};
        [filename pathname] = uigetfile({ ...
            '*.vff', 'All VFF Files (*.vff)'; ...
            '*.*', 'All Files (*.*)' ...
            },'Please select a vff file', varargin{2});
    case 3
        showimage = varargin{1};
        pathname = varargin{2};
        filename = varargin{3};
    otherwise
        error('VFFFileReader:ArgumentError', ...
            'ArgumentError:  Too many arguments specified');
end

fid = fopen(fullfile(pathname, filename), 'r', 'b');

done = false;
decimalRegex = '[-+]?\d+(\.|(\.\d+))?';

header = struct('Path', {}, 'Filename', {}, 'Rows', {}, ...
    'Columns', {}, 'Bits', {});
header(1).Path = pathname;
header(1).Filename = filename;

while (~done)
    line = fgetl(fid);
    if (showimage)
        disp(line);
    end
    
    % header terminates with a form-feed character 0x0C (12 in decimal)
    if isempty(line) || ~isempty(find(uint8(line) == 12, 1))
        done = true;
        continue
    end
    
    temp = regexp(line, '^size=\s*(?<rows>\d+)\s+(?<columns>\d+)\s*;\s*$', 'names');
    if ~isempty(temp)
        header(1).Rows = str2double(temp.rows);
        header(1).Columns = str2double(temp.columns);
        continue
    end
    
    temp = regexp(line, '^bits=\s*(?<bits>\d+)\s*;\s*$', 'names');
    if ~isempty(temp)
        header(1).Bits = str2double(temp.bits);
        continue
    end
    
    temp = regexp(line, '^rank=\s*(?<rank>\d+)\s*;\s*$', 'names');
    if ~isempty(temp)
        header(1).Rank = str2double(temp.rank);
        continue
    end
    
    temp = regexp(line, '^type=\s*(?<type>.+)\s*;\s*$', 'names');
    if ~isempty(temp)
        header(1).Type = temp.type;
        continue
    end
    
    temp = regexp(line, '^rawsize=\s*(?<rawsize>\d+)\s*;\s*$', 'names');
    if ~isempty(temp)
        header(1).RawSize = str2double(temp.rawsize);
        continue
    end
    
    temp = regexp(line, horzcat('^spacing=\s*', ...
        '(?<spacing1>', decimalRegex, ')\s+', ...
        '(?<spacing2>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        header(1).Spacing = [str2double(temp.spacing1) str2double(temp.spacing2)];
        continue
    end
    
    temp = regexp(line, horzcat('^origin=\s*', ...
        '(?<origin1>', decimalRegex, ')\s+', ...
        '(?<origin2>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        header(1).Origin = [str2double(temp.origin1) str2double(temp.origin2)];
        continue
    end
    
    temp = regexp(line, '^bands=\s*(?<bands>\d+)\s*;\s*$', 'names');
    if ~isempty(temp)
        header(1).Bands = str2double(temp.bands);
        continue
    end
    
    temp = regexp(line, '^format=\s*(?<format>.+)\s*;\s*$', 'names');
    if ~isempty(temp)
        header(1).Format = temp.format;
        continue
    end
    
    temp = regexp(line, horzcat('^elementsize=\s*', ...
        '(?<elementsize>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        header(1).ElementSize = str2double(temp.elementsize);
        continue
    end
    
    temp = regexp(line, horzcat('^water=\s*', ...
        '(?<water>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        header(1).Water = str2double(temp.water);
        continue
    end
    
    temp = regexp(line, horzcat('^air=\s*', ...
        '(?<air>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        header(1).Air = str2double(temp.air);
        continue
    end
    
    temp = regexp(line, horzcat('^boneHU=\s*', ...
        '(?<boneHU>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        header(1).BoneHU = str2double(temp.boneHU);
        continue
    end
    
    temp = regexp(line, '^subject=\s*(?<subject>\w+)\s*;\s*$', 'names');
    if ~isempty(temp)
        header(1).Subject = temp.subject;
        continue
    end
    
    temp = regexp(line, '^title=\s*(?<title>\w+)\s*;\s*$', 'names');
    if ~isempty(temp)
        header(1).Title = temp.title;
        continue
    end
    
    temp = regexp(line, '^date=\s*(?<date>.+);\s*$', 'names');
    if ~isempty(temp)
        header(1).Date = temp.date;
        continue
    end
    
    temp = regexp(line, '^binning=\s*(?<binning>.+);\s*$', 'names');
    if ~isempty(temp)
        header(1).Binning = temp.binning;
        continue
    end
    
    temp = regexp(line, '^voltage=\s*(?<voltage>\d+)\s*;\s*$', 'names');
    if ~isempty(temp)
        header(1).Voltage = str2double(temp.voltage);
        continue
    end
    
    temp = regexp(line, horzcat('^current=\s*', ...
        '(?<current>', decimalRegex, ')\s*;\s*$'), 'names');
    if ~isempty(temp)
        header(1).Current = str2double(temp.current);
        continue
    end
end
rows = header.Rows;
columns = header.Columns;
bits = header.Bits;
status = fseek(fid, -rows*columns*bits/8,'eof');

% fseek returns 0 on success
if status == 0
    image = fread(fid,[rows, columns],'int16');
else
    error('VFFFileReader:FileFormatError', ...
        'FileFormatError:  Could not read image data');
end
fclose(fid);

if showimage
    h = figure;
    imshow(image,[]);
    colormap(h, gray);
    set(h, 'Name', filename);
end