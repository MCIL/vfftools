This project modifies "Micro-CT VFF reader" and "VFF reader (3D)" from the 
MATLAB FileExchange:
http://www.mathworks.com/matlabcentral/fileexchange/7475-micro-ct-vff-reader and
http://www.mathworks.com/matlabcentral/fileexchange/20853-vff-reader--3d-

The vff function is modified to support both interactive and programmatic 
selection of the file to be read. ASCII file header information is extracted
and provided as a struct in the output. Display of the image may be suppressed
if desired.

The vff3D function is also modified to support interactive and programmatic 
modes of use. The contents of the ASCII file header and the volumetric image 
are provided as a struct in the output. Volume dimensions are permuted to 
correct for the mismatch between the row major order representation of the image
as written by the MicroCT software and the column major order representation in
MATLAB.

A new vffwrite function is provided to permit writing 2D grayscale images in VFF
format. A new FilterMicroCtProjections function was created with the intent of
improving image quality by applying a smoothing filter to the 2D projection 
images prior to performing Feldkamp-Davis-Kress reconstruction using software
on the MicroCT console.

Uploaded 5/6/2016
-Matt Latourette