function [ output_args ] = FilterMicroCtProjections(sourcepath, destinationpath)
%   This program reads a series of 2D VFF files representing the 
%   projections of a micro CT acquisition, filters these images, and writes
%   the filtered images to the destination folder.
%
%   Matt Latourette, 6/9/2015
%   Department of Radiology
%   Michigan State University
%
%   [ output_args ] = FilterMicroCtProjections(sourcepath, destinationpath)

if (strcmpi(fullfile(sourcepath), fullfile(destinationpath)))
    error('FilterMicroCtProjections:ArgumentError', ...
        'ArgumentError:  Source and destination paths cannot be the same');
end

if (~exist(fullfile(destinationpath)))
    mkdir(fullfile(destinationpath));
end

filelist = dir(fullfile(sourcepath, '*.vff'));

numfiles = size(filelist, 1);
for (i=1:numfiles)
    [image, header] = vff(0, sourcepath, filelist(i).name);
    outimage = medfilt2(image, [3 3], 'symmetric');
%     figure(1);
%     subplot(1, 2, 1); imshow(image, []);
%     title('Original');
%     subplot(1, 2, 2); imshow(outimage, []);
%     title('Filtered');
    vffwrite(outimage, header, destinationpath, filelist(i).name);
end